#!/bin/bash
# Cloudflare as Dynamic DNS
# Based on: https://gist.github.com/benkulbertis/fff10759c2391b6618dd/

# Update these with real values
auth_email="email@example.com"
auth_key="global_api_key_goes_here" 
zone_name="example.com"
record_name="home.example.com"

# Begin Program
ip=$(hostname -I | cut -f 1 -d ' ')
ip_file="ip.txt"

# Keep files in the same folder when run from cron
current="$(pwd)"
cd "$(dirname "$(readlink -f "$0")")"

if [ -f $ip_file ]; then
    old_ip=$(cat $ip_file)
    if [ $ip == $old_ip ]; then
        echo "IP has not changed. Aborting"
        exit 0
    fi
fi
zone_identifier=$(curl -s -X GET "https://api.cloudflare.com/client/v4/zones?name=$zone_name" -H "X-Auth-Email: $auth_email" -H "X-Auth-Key: $auth_key" -H "Content-Type: application/json" | jq -r '.result[0].id')
    record_identifier=$(curl -s -X GET "https://api.cloudflare.com/client/v4/zones/$zone_identifier/dns_records?name=$record_name" -H "X-Auth-Email: $auth_email" -H "X-Auth-Key: $auth_key" -H "Content-Type: application/json" | jq -r '.result[0].id')
    echo "$zone_identifier" > $id_file
# Uncomment for Debug purposes
# echo "Zone ID is $zone_identifier"
# echo "Record ID is $record_identifier"

update=$(curl -s -X PUT "https://api.cloudflare.com/client/v4/zones/$zone_identifier/dns_records/$record_identifier" -H "X-Auth-Email: $auth_email" -H "X-Auth-Key: $auth_key" -H "Content-Type: application/json" --data "{\"id\":\"$zone_identifier\",\"type\":\"A\",\"name\":\"$record_name\",\"content\":\"$ip\"}")


if [[ $update == *"\"success\":false"* ]]; then
    #Uncomment to debug
    # message="API UPDATE FAILED. DUMPING RESULTS:\n$update"
    # echo -e "$message"
    exit 1 
else
    #Uncomment to debug
    # message="IP changed to: $ip"
    echo "$ip" > $ip_file
    # echo "$message"
fi




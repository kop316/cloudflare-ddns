# cloudflare-ddns
simple script to update cloudflares DDNS

Based on: https://gist.github.com/benkulbertis/fff10759c2391b6618dd/

To Run every minute
* * * * * /root/cloudflare-ddns/cloudflare-ddns.sh >/dev/null 2>&1
